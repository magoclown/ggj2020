﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[RequireComponent(typeof(Rigidbody2D))]
public class Explodable : MonoBehaviour
{
    public System.Action<List<GameObject>> OnFragmentsGenerated;

    public bool allowRuntimeFragmentation = false;
    public int extraPoints = 0;
    public int subshatterSteps = 0;

    public string fragmentLayer = "Default";
    public string sortingLayerName = "Default";
    public int orderInLayer = 0;

    private Transform fragmentParent;

    public enum ShatterType
    {
        Triangle,
        Voronoi
    };
    public ShatterType shatterType;
    public List<GameObject> fragments = new List<GameObject>();
    private List<List<Vector2>> polygons = new List<List<Vector2>>();

    public Color originalColor;
    public Color unableColor;

    public enum StatusExplodable
    {
        Complete,
        Destroy
    };
    public StatusExplodable startBroken;

    public StatusExplodable actualStatus;

    void Start() {
        
        fragmentParent = GameObject.Find("Fragments").transform;
        originalColor = this.GetComponent<SpriteRenderer>().color;
        if (startBroken == StatusExplodable.Destroy)
            explode();
    }

    /// <summary>
    /// Creates fragments if necessary and destroys original gameobject
    /// </summary>
    public void explode()
    {
        if(actualStatus == StatusExplodable.Complete) { 
            //if fragments were not created before runtime then create them now
            if (fragments.Count == 0 && allowRuntimeFragmentation)
            {
                generateFragments();
            }
            //otherwise unparent and activate them
            else
            {
                foreach (GameObject frag in fragments)
                {
                    frag.transform.parent = fragmentParent;
                    frag.SetActive(true);
                }
            }
            //if fragments exist destroy the original
            if (fragments.Count > 0)
            {
                //Destroy(gameObject);
                this.GetComponent<Rigidbody2D>().isKinematic = true;
                this.GetComponent<SpriteRenderer>().color = unableColor;
                this.GetComponent<Collider2D>().isTrigger = true;
            }
            if (startBroken == StatusExplodable.Destroy)
            {
                startBroken = StatusExplodable.Complete;
            }
            else {
                ActionsManager.instance.AddAction();
            }
                
            actualStatus = StatusExplodable.Destroy;
        }
    }
    /// <summary>
    /// Creates fragments and then disables them
    /// </summary>
    public void fragmentInEditor()
    {
        if (fragments.Count > 0)
        {
            deleteFragments();
        }
        generateFragments();
        setPolygonsForDrawing();
        foreach (GameObject frag in fragments)
        {
            frag.transform.parent = transform;
            frag.SetActive(false);
        }
    }
    public void deleteFragments()
    {
        foreach (GameObject frag in fragments)
        {
            frag.GetComponent<FragmentRegen>().StopAll();
            if (Application.isEditor)
            {
                DestroyImmediate(frag);
            }
            else
            {
                Destroy(frag);
            }
        }
        fragments.Clear();
        polygons.Clear();
    }
    /// <summary>
    /// Turns Gameobject into multiple fragments
    /// </summary>
    private void generateFragments()
    {
        fragments = new List<GameObject>();
        switch (shatterType)
        {
            case ShatterType.Triangle:
                fragments = SpriteExploder.GenerateTriangularPieces(gameObject, extraPoints, subshatterSteps);
                break;
            case ShatterType.Voronoi:
                fragments = SpriteExploder.GenerateVoronoiPieces(gameObject, extraPoints, subshatterSteps);
                break;
            default:
                Debug.Log("invalid choice");
                break;
        }
        //sets additional aspects of the fragments
        foreach (GameObject p in fragments)
        {
            if (p != null)
            {
                p.transform.parent = fragmentParent;
                p.layer = LayerMask.NameToLayer(fragmentLayer);
                p.GetComponent<Renderer>().sortingLayerName = sortingLayerName;
                p.GetComponent<Renderer>().sortingOrder = orderInLayer;
                p.AddComponent<FragmentRegen>();
                p.GetComponent<FragmentRegen>().origTransform = p.transform.position;
            }
        }

        foreach (ExplodableAddon addon in GetComponents<ExplodableAddon>())
        {
            if (addon.enabled)
            {
                addon.OnFragmentsGenerated(fragments);
            }
        }
    }
    private void setPolygonsForDrawing()
    {
        polygons.Clear();
        List<Vector2> polygon;

        foreach (GameObject frag in fragments)
        {
            polygon = new List<Vector2>();
            foreach (Vector2 point in frag.GetComponent<PolygonCollider2D>().points)
            {
                Vector2 offset = rotateAroundPivot((Vector2)frag.transform.position, (Vector2)transform.position, Quaternion.Inverse(transform.rotation)) - (Vector2)transform.position;
                offset.x /= transform.localScale.x;
                offset.y /= transform.localScale.y;
                polygon.Add(point + offset);
            }
            polygons.Add(polygon);
        }
    }
    private Vector2 rotateAroundPivot(Vector2 point, Vector2 pivot, Quaternion angle)
    {
        Vector2 dir = point - pivot;
        dir = angle * dir;
        point = dir + pivot;
        return point;
    }

    void OnDrawGizmos()
    {
        if (Application.isEditor)
        {
            if (polygons.Count == 0 && fragments.Count != 0)
            {
                setPolygonsForDrawing();
            }

            Gizmos.color = Color.blue;
            Gizmos.matrix = transform.localToWorldMatrix;
            Vector2 offset = (Vector2)transform.position * 0;
            foreach (List<Vector2> polygon in polygons)
            {
                for (int i = 0; i < polygon.Count; i++)
                {
                    if (i + 1 == polygon.Count)
                    {
                        Gizmos.DrawLine(polygon[i] + offset, polygon[0] + offset);
                    }
                    else
                    {
                        Gizmos.DrawLine(polygon[i] + offset, polygon[i + 1] + offset);
                    }
                }
            }
        }
    }
    private bool m_isRestoring = false;
    public IEnumerator Restore()
    {
        if (actualStatus == StatusExplodable.Destroy)
        {
            if (!m_isRestoring)
            {
                m_isRestoring = true;
                foreach (GameObject frag in fragments)
                {
                    if (frag != null)
                        if (frag.GetComponent<FragmentRegen>())
                            StartCoroutine(frag.GetComponent<FragmentRegen>().Restore());
                }

                yield return new WaitForSeconds(0.5f);

                deleteFragments();

                this.GetComponent<Rigidbody2D>().isKinematic = false;
                // this.GetComponent<SpriteRenderer>().enabled = true;
                this.GetComponent<SpriteRenderer>().color = originalColor;
                this.GetComponent<Collider2D>().isTrigger = false;
                m_isRestoring = false;
                ActionsManager.instance.RemoveAction();
            }
            actualStatus = StatusExplodable.Complete;
        }
    }
    

}
