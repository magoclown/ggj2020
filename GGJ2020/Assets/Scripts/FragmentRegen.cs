﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentRegen : MonoBehaviour
{
    public Vector3 origTransform;
    public float timeTo = 1;
    public IEnumerator Restore(){
        float rate = 1 / timeTo;
        float alpha = 0;
        //while (Vector3.Distance(transform.position, origTransform) > 0.5f)
        //GetComponent<PolygonCollider2D>().enabled = false;
        while (transform != null && alpha < 1.0f)
        {
            //transform.position = Vector3.MoveTowards(transform.position, origTransform, 1.0f);
            transform.position = Vector3.MoveTowards(transform.position, origTransform, alpha);
            alpha += rate * Time.deltaTime;
            yield return null;
        }
        transform.position = Vector3.MoveTowards(transform.position, origTransform, 1);
    }
    public void StopAll()
    {
        StopAllCoroutines();
    }
}
