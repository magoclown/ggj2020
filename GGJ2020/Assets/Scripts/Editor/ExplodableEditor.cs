﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(Explodable))]
public class ExplodableEditor : Editor {

    Explodable myTarget;
    SerializedObject GetTarget;

    private void OnEnable() {
        myTarget = (Explodable)target;
        GetTarget = new SerializedObject(myTarget);
    }

    public override void OnInspectorGUI()
    {
        // GetTarget.Update();
        SerializedProperty startBroken = GetTarget.FindProperty("startBroken");
        SerializedProperty allowRuntimeFragmentation = GetTarget.FindProperty("allowRuntimeFragmentation");
        SerializedProperty shatterType = GetTarget.FindProperty("shatterType");
        SerializedProperty extraPoints = GetTarget.FindProperty("extraPoints");
        SerializedProperty subshatterSteps = GetTarget.FindProperty("subshatterSteps");
        
        SerializedProperty fragmentLayer = GetTarget.FindProperty("fragmentLayer");
        SerializedProperty sortingLayerName = GetTarget.FindProperty("sortingLayerName");
        SerializedProperty orderInLayer = GetTarget.FindProperty("orderInLayer");
        SerializedProperty originalColor = GetTarget.FindProperty("originalColor");
        SerializedProperty unableColor = GetTarget.FindProperty("unableColor");
        //SerializedProperty startBroken = GetTarget.FindProperty("startBroken");
        EditorGUILayout.PropertyField(startBroken);
        EditorGUILayout.PropertyField(allowRuntimeFragmentation);
        EditorGUILayout.PropertyField(shatterType);
        EditorGUILayout.PropertyField(extraPoints);
        EditorGUILayout.PropertyField(subshatterSteps);
        if (subshatterSteps.intValue > 1)
        {
            EditorGUILayout.HelpBox("Use subshatter steps with caution! Too many will break performance!!! Don't recommend more than 1", MessageType.Warning);
        }
        EditorGUILayout.PropertyField(fragmentLayer);
        EditorGUILayout.PropertyField(sortingLayerName);
        EditorGUILayout.PropertyField(orderInLayer);
        EditorGUILayout.PropertyField(originalColor);
        EditorGUILayout.PropertyField(unableColor);
        // Explodable myTarget = (Explodable)target;
        //myTarget.startBroken = (Explodable.StatusExplodable)EditorGUILayout.EnumPopup("Status Explodable", myTarget.startBroken);
        //myTarget.allowRuntimeFragmentation = EditorGUILayout.Toggle("Allow Runtime Fragmentation", myTarget.allowRuntimeFragmentation);
        //myTarget.shatterType = (Explodable.ShatterType)EditorGUILayout.EnumPopup("Shatter Type", myTarget.shatterType);
        //myTarget.extraPoints = EditorGUILayout.IntField("Extra Points", myTarget.extraPoints);
        //myTarget.subshatterSteps = EditorGUILayout.IntField("Subshatter Steps",myTarget.subshatterSteps);
        //if (myTarget.subshatterSteps > 1)
        //{
        //    EditorGUILayout.HelpBox("Use subshatter steps with caution! Too many will break performance!!! Don't recommend more than 1", MessageType.Warning);
        //}

        //myTarget.fragmentLayer = EditorGUILayout.TextField("Fragment Layer", myTarget.fragmentLayer);
        //myTarget.sortingLayerName = EditorGUILayout.TextField("Sorting Layer", myTarget.sortingLayerName);
        //myTarget.orderInLayer = EditorGUILayout.IntField("Order In Layer", myTarget.orderInLayer);

        //myTarget.originalColor = EditorGUILayout.ColorField("Original Color", myTarget.originalColor);
        //myTarget.unableColor = EditorGUILayout.ColorField("Destroyed Color", myTarget.unableColor);



        if (myTarget.GetComponent<PolygonCollider2D>() == null && myTarget.GetComponent<BoxCollider2D>() == null)
        {
            EditorGUILayout.HelpBox("You must add a BoxCollider2D or PolygonCollider2D to explode this sprite", MessageType.Warning);
        }
        else
        {
            if (GUILayout.Button("Generate Fragments"))
            {
                myTarget.fragmentInEditor();
                EditorUtility.SetDirty(myTarget);
            }
            if (GUILayout.Button("Destroy Fragments"))
            {
                myTarget.deleteFragments();
                EditorUtility.SetDirty(myTarget);
            }
        }
        GetTarget.ApplyModifiedProperties();
    }
}
