﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StringLoad : MonoBehaviour
{



    public InputField inputPass;
    public InputField inputChannel;
    public InputField inputName;

public void Start()
{

  inputPass.text =  GetPassword();
    inputChannel.text =  GetChannel();
      inputName.text =  GetUserName();
}

    public string GetPassword()
    {
      return GameManager.instance.password;
    }

    public string GetChannel()
    {
        return GameManager.instance.channelName;
    }

    public string GetUserName()
    {
      return  GameManager.instance.userName;
    }
}
