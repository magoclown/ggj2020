﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Explodable))]
public class ExplodeOnClick : MonoBehaviour {

	private Explodable _explodable;
	public bool destroyed;

	void Start()
	{
		destroyed = false;
		_explodable = GetComponent<Explodable>();
	}
	void OnMouseDown()
	{
		if (!destroyed)
		{
			destroyed = true;
			_explodable.explode();
			ExplosionForce ef = GameObject.FindObjectOfType<ExplosionForce>();
			ef.doExplosion(transform.position);

		}
		else {

			destroyed = false;
			StartCoroutine(_explodable.Restore()); 
		}
		
	}
}
