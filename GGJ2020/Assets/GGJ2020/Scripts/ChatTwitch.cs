﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChatTwitch : MonoSingleton<ChatTwitch>
{
    public Text chat;
    public Scrollbar scroll;
    public bool callClear = false;
    public void AddText(string msg) {
        chat.text += msg + Environment.NewLine;
        if(scroll)
        scroll.value = 0;
        if(!callClear)
            StartCoroutine(Clear());
    }

    IEnumerator Clear() {
        callClear = true;
        yield return new WaitForSeconds(2);
        chat.text = "";
        callClear = false;
    }
    
}
