﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonHandler : MonoBehaviour
{

      public void playGame()
      {
          SceneManager.LoadScene ("LevelSelect");
          GameManager.instance.isVoteCount = 1;
      }

      public void twitchSettings()
      {
          SceneManager.LoadScene ("TwitchSettings");
      }

}
