﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoSingleton<InputController>
{
    public bool canMove = true;
    public float horizontal;
    public bool jump;
    public bool repair;
    public bool breaker;
    public float mirror = 1;
    public float speed = 1;
    void Update()
    {
        CheckInputs();        
    }

    public void CheckInputs() {
        if (!canMove) {
            horizontal = 0;
            jump = false;
            repair = false;
            breaker = false;
            return;   
        }
        horizontal = Input.GetAxis("Horizontal") * mirror * speed;
        jump = Input.GetButtonDown("Jump");
        repair = Input.GetButton("Repair");
        breaker = Input.GetButton("Breaker");
    }

    public void MirrorController() {
        mirror *= -1;
    }
    public void SpeedController() {
        if (speed == 1)
            speed = 2;
        else
            speed = 1;
    }
}
