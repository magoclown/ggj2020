﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingButtonHandler : MonoBehaviour
{

    public InputField userNameIF;
    public InputField passwordIF;
    public InputField channelNameIF;
    // Start is called before the first frame update
    public void back()
    {
        SceneManager.LoadScene ("PantallaInicio");
    }

    public void connectSettings()
    {
      if(userNameIF.text != "" && passwordIF.text != "" && channelNameIF.text != ""){
        GameManager.instance.userName = userNameIF.text;
        GameManager.instance.password = passwordIF.text;
        GameManager.instance.channelName = channelNameIF.text;

        Debug.Log(GameManager.instance.userName);
        Debug.Log(GameManager.instance.password);
        Debug.Log(GameManager.instance.channelName);
        Debug.Log(GameManager.instance.firstTime);
        GameManager.instance.connect();
        GameManager.instance.firstTime = true;
      }
    }
}
