﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;

    void Awake() {
        if (player == null)
            player = GameObject.Find("PacoCharacter").transform;
    }

    void Update()
    {
        this.transform.position = player.position;
    }
}
