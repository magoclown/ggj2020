﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : PhysicsObject
{
    public ActionCollider actionControllerL;
    public ActionCollider actionControllerR;

    public InputController input;
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    bool lastSide;
    bool actualSide;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }
    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;

        move.x = input.horizontal;

        if (input.jump && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
        bool flipSprite = actualSide;
        if (move.x > 0.1f)
            flipSprite = true;
        else if(move.x < -0.1f)
            flipSprite = false;
        // bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (actualSide != flipSprite)
        {
            lastSide = actualSide;
            actualSide = flipSprite;
        }
        spriteRenderer.flipX = !actualSide;
        if (actualSide)
        {
            // spriteRenderer.flipX = !spriteRenderer.flipX;
            actionControllerR.actionRepair = input.repair;
            actionControllerR.actionBreak = input.breaker;
        }
        else {
            actionControllerL.actionRepair = input.repair;
            actionControllerL.actionBreak = input.breaker;
        }
       

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);
        animator.SetBool("break", input.repair);
        animator.SetBool("repair", input.breaker);
        targetVelocity = move * maxSpeed;
    }
}
