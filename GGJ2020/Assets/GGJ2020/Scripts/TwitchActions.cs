﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwitchActions : MonoBehaviour
{
  public int speed, controls;

    public void CatMessage() {
        Debug.Log("!CatMessage");
    }

    public void Explode() {
        Debug.Log("!Explode");
    }

    public void RageQuit() {
        Debug.Log("!RageQUit");
    }

    public void Speed() {
        Debug.Log("!Speed");
        if(GameManager.instance.isVoteRound == true){
          speed ++;
          GameManager.instance.speed = speed;
        }
    }

    public void Controls() {
        Debug.Log("!Controls");
        if(GameManager.instance.isVoteRound == true){
          controls ++;
          GameManager.instance.controls = controls;
        }
    }
}
