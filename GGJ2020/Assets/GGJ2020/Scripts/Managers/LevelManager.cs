﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoSingleton<LevelManager>
{
    public GameObject panelVictory;
    public void Victory() {
        panelVictory.SetActive(true);
        InputController.instance.canMove = false;
    }
}
