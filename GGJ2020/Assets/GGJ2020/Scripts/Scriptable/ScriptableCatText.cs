﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CatString", menuName = "ScriptableObjects/CatString", order = 1)]
public class ScriptableCatText : ScriptableObject   
{
    public List<string> phrases;

    public string GetRandomString() {
        int select = Random.Range(0, phrases.Count);
        
        return phrases[select];
    }
}
