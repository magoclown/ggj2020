﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

[Serializable]
public class ListTwitch {

    public ListTwitch() {
        BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;
        Type myType = typeof(TwitchActions);
        methods = new List<string>();
        foreach (var method in myType.GetMethods(flags))
        {
            methods.Add(method.ToString());
        }

        
    }
    public string command;
    
    public List<string> methods;
}

[CreateAssetMenu(fileName = "TwitchActions", menuName = "ScriptableObjects/TwitchActions", order = 1)]
public class ScriptableTwitch : ScriptableObject
{
    public List<ListTwitch> actions = new List<ListTwitch>();
    public List<string> methods;
    public List<int> selected = new List<int>();
    public Dictionary<string, ListTwitch> dic;

    public void Prepare() {
        dic = new Dictionary<string, ListTwitch>();
        foreach (var action in actions) {
            dic.Add(action.command, action);
        }
    }

    public string GetSelectedMethod(string command) {
        int sel = actions.IndexOf(dic[command]);
        return actions[sel].methods[selected[sel]];
    }

    void OnEnable() {
        BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;
        Type myType = typeof(TwitchActions);
        // actions = new List<ListTwitch>();
        methods = new List<string>();
        // selected = new List<int>();
        foreach (var method in myType.GetMethods(flags))
        {
            methods.Add(method.ToString());
        }
    }

    public void AddElement() {
        selected.Add(0);
        actions.Add(new ListTwitch());
    }
    public void RemoveElement() {
        selected.RemoveAt(selected.Count - 1);
        actions.RemoveAt(actions.Count - 1);
    }
}
