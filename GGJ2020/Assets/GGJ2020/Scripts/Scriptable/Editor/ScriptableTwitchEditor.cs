﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.AnimatedValues;

[CustomEditor(typeof(ScriptableTwitch))]
public class ScriptableTwitchEditor : Editor {
    List<bool> showPositions;
    ScriptableTwitch myTarget;
    SerializedObject GetTarget;
    SerializedProperty ThisList;
    
    private void OnEnable()
    {
        showPositions = new List<bool>();
        myTarget = (ScriptableTwitch)target;
        GetTarget = new SerializedObject(myTarget);
        ThisList = GetTarget.FindProperty("actions");
        foreach (var action in myTarget.actions) {
            showPositions.Add(false);
        }
    }

    public override void OnInspectorGUI()
    {
        GetTarget.Update();
        EditorGUILayout.TextField("Count", showPositions.Count.ToString());
        EditorGUILayout.BeginHorizontal("Button");
            if (GUILayout.Button("Add Action")) {
                myTarget.AddElement();
                showPositions.Add(true);
            }
            if (GUILayout.Button("Remove Action"))
            {
                myTarget.RemoveElement();
                showPositions.RemoveAt(showPositions.Count - 1);
            }
        EditorGUILayout.EndHorizontal();
        if (myTarget.actions.Count > 0) { 
            int i = 0;
            foreach (SerializedProperty item in ThisList)
            {
                AnimBool showExtraFields;
                showExtraFields = new AnimBool(true);
                showExtraFields.valueChanged.AddListener(Repaint);

                SerializedProperty command = item.FindPropertyRelative("command");
                SerializedProperty methods = item.FindPropertyRelative("methods");
                SerializedProperty selected = item.FindPropertyRelative("selected");

                if ((showPositions.Count - 1) < i) {
                    break;
                }

                showPositions[i] = EditorGUILayout.Foldout(showPositions[i], command.stringValue);
                if (showPositions[i]) {
                    EditorGUILayout.PropertyField(command);
                    myTarget.selected[i] = EditorGUILayout.Popup(myTarget.selected[i], myTarget.methods.ToArray());
                }
                i++;
            }
        }
        GetTarget.ApplyModifiedProperties();
    }
}
