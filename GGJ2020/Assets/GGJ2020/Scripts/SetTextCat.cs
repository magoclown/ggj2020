﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SetTextCat : MonoBehaviour
{
    public ScriptableCatText texts;
    public Text text;
    
    void Start()
    {
        text.text = texts.GetRandomString();
    }

}
