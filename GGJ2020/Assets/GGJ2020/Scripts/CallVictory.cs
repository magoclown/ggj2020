﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallVictory : MonoBehaviour
{
    public CharacterController player;

    void Start() {
        player = GameObject.Find("PacoCharacter").GetComponent<CharacterController>();
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            player.rb2d.Sleep();
            LevelManager.instance.Victory();
        }
    }
    
}
