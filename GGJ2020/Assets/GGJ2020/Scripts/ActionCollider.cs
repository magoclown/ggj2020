﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCollider : MonoBehaviour
{
    public string tagToCompare = "Obstacle";
    public bool actionRepair;
    public bool actionBreak;
    public ActionsManager actions;

    private ExplosionForce m_ExplosionForce;

    void Start() {
        m_ExplosionForce = GameObject.FindObjectOfType<ExplosionForce>();
    }
    void Action(Collider2D other) {
        if (other.CompareTag(tagToCompare))
        {
            var explodable = other.GetComponent<Explodable>();
            if (explodable)
            {
                
                if (actionBreak)
                {
                    explodable.explode();

                    if (m_ExplosionForce)
                        m_ExplosionForce.doExplosion(explodable.transform.position);
                }
                if (actionRepair && ActionsManager.instance.GetActions() > 0)
                {
                    StartCoroutine(explodable.Restore());
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        Action(other);
    }

    void OnTriggerStay2D(Collider2D other) {
        Action(other);
    }
}
