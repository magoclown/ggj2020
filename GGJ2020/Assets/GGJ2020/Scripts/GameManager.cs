﻿using System;
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;
using System.Net.Sockets;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoSingleton<GameManager>
{
  private TcpClient twitchClient;
  private StreamReader reader;
  private StreamWriter writer;
  public bool firstTime = false;
  public int isVoteCount;
  public bool isVoteRound = false;
  public int speed, controls;

  public Button connectButton;
  public Button ConnectButton{
    get{
      if(connectButton == null)
      {
        GameObject bt = GameObject.Find("ValorStatus");
        if(bt)
          connectButton = bt.GetComponent<Button>();
      }
      return connectButton;
    }
    set{
      connectButton = value;
    }
  }

  public Text statusText;
  public Text StatusText{
    get{
      if(statusText == null)
      {
        GameObject st = GameObject.Find("ValorStatus");
        if(st)
          statusText = st.GetComponent<Text>();
      }
      return statusText;
    }
    set{
      statusText = value;
    }
  }
  public string userName, password, channelName;


  void Start() {


  }

    // Update is called once per frame
    void Update()
    {
      if(isVoteCount > 0 && isVoteCount < 1500){
        isVoteRound = true;
        isVoteCount ++;
      }else{
        isVoteRound = false;
        isVoteCount = 0;
            speed = 0;
            controls = 0;
      }

      if((speed > controls)){
        Debug.Log("Speed" + speed);
            InputController.instance.SpeedController();
        }
        else if((speed < controls)){
        Debug.Log("controls" + controls);
            InputController.instance.MirrorController();
      }

      if(firstTime == true){
        if (!twitchClient.Connected){
          if(StatusText){
              StatusText.text = "Disconnected";
              if(ConnectButton)
                ConnectButton.interactable = true;
            }
              connect();
            }
            if(StatusText)
                StatusText.text = "Connected";
                if(ConnectButton)
                  ConnectButton.interactable = false;
              ReadChat();

          }else{
            if(StatusText)
              StatusText.text = "Disconnected";
              if(ConnectButton)
                ConnectButton.interactable = true;
          }
    }

    public void connect()
    {
      twitchClient = new TcpClient("irc.chat.twitch.tv", 6667);
      reader = new StreamReader(twitchClient.GetStream());
      writer = new StreamWriter(twitchClient.GetStream());

      writer.WriteLine("PASS " + password);
      writer.WriteLine("NICK " + userName);
      writer.WriteLine("USER " + userName + " 8 * :" + userName);
      writer.WriteLine("JOIN #" + channelName);

      Debug.Log("Llego aqui");
      writer.Flush();
    }

    private void ReadChat()
    {
      // Debug.Log("Ya voy aqui");
        if (twitchClient.Available > 0)
        {
            var message = reader.ReadLine();
            if (message.Contains("PRIVMSG"))
            {
                Scene scene = SceneManager.GetActiveScene();
                // ChatTwitch.instance.AddText("Connected");
                var splitPoint = message.IndexOf("!", 1);
                var chatName = message.Substring(0, splitPoint);
                chatName = chatName.Substring(1);
                splitPoint = message.IndexOf(":", 1);
                message = message.Substring(splitPoint + 1);
                print(String.Format("{0}: {1}", chatName, message));
                if (scene.name.Contains("Level")) {
                    TwitchManager.instance.GetMessage(message);
                }
                ChatTwitch.instance.AddText(String.Format("{0}: {1}", chatName, message));
                //GameInputs(message);
            }
        }
    }
}
