﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionsManager : MonoSingleton<ActionsManager>
{
    [SerializeField]
    private int actions;

    public void AddAction(int value = 1) {
        actions += value;
    }

    public void RemoveAction(int value = 1)
    {
        actions -= value;
    }

    public int GetActions()
    {
        return actions;
    }
}
