﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;
using System.Linq;

public class TwitchManager : MonoSingleton<TwitchManager>
{
    public ScriptableTwitch config;
    public TwitchActions actions;

    void Start() {
        if (config != null) {
            config.Prepare();
        }
        // Invoke("GetMessage", 2);
    }

    public void GetMessage(string msg = "!Cat") {
        if(config != null) { 
            if (config.dic.ContainsKey(msg)) {
                BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;
                Type myType = typeof(TwitchActions);
                string methodName = config.GetSelectedMethod(msg).Replace("Void", "").Replace("()", "").Trim();
                MethodInfo method = myType.GetMethod(methodName) ;
                actions.Invoke(methodName, 0);
            }
        }
        // else {
        //  actions.Invoke(msg, 0);
        //}
    }
}
